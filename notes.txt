image: trion/ng-cli-karma

cache:
  paths:
    - node_modules/

deploy_stage:
  stage: deploy
  environment: Stage
  only:
    - master
  script:
    - rm ./package-lock.json
    - npm install
    - ./node_modules/@angular/cli/bin/ng test --progress false --single-run=true --watch=false
    - ./node_modules/@angular/cli/bin/ng e2e --progress false --watch=false
    - ./node_modules/@angular/cli/bin/ng build --progress false --prod --base-href XXX-stage.surge.sh
    - ./node_modules/.bin/surge -p dist/ --domain XXX-stage.surge.sh



    =======

JAVA DEPLOY 

    image: java:8

stages:
  - build
  - deploy

build:
  stage: build
  script: ./gradlew assemble
  artifacts:
    paths:
      - build/libs/helloworld.jar

production:
  stage: deploy
  script:
  - curl --location "https://cli.run.pivotal.io/stable?release=linux64-binary&source=github" | tar zx
  - ./cf login -u $CF_USERNAME -p $CF_PASSWORD -a api.run.pivotal.io
  - ./cf push
  only:
  - master